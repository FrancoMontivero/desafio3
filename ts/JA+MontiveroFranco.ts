async function travelText(text: string, callback: (amountWords: number) => void, time: number=1000): Promise<any> {
  const words = text.split(' ');
  for(let word of words){
    await new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(console.log(word));
      }, time);
    })
  }
  callback(words.length);
}

async function main(): Promise<any>{
  let totalWords = 0;
  const counterWords: (amountWords: number) => void = (amountWords: number) => { totalWords += amountWords };
  await travelText('Lorem ipsum dolor sit amet consectetur adipisicing', counterWords);
  await travelText('Ut enim ad minim veniam', counterWords);
  await travelText('ed do eiusmod tempor incididunt', counterWords)
  console.log(`Proceso completo`);
  console.log(`Cantidad de palabras: ${totalWords}`);
}

export = main;
